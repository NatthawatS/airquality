import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:air_quality/models/maps.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Completer<GoogleMapController> _controller = Completer();

  List<Data> locations = [];
  bool isLoading = true;
  Icon cusIcon = Icon(Icons.search);
  Widget locationsSearchBar = Text("Air Quality");
  var searchBar = '';
  LatLng currentPostion;

  _getData(String searchBar) async {
    var url = searchBar.isEmpty ?? true
        ? 'https://api.waqi.info/search/?keyword=bangkok&token=c95f5806ce310e72d234dae550aa15c54e123f82'
        : 'https://api.waqi.info/search/?keyword=$searchBar&token=c95f5806ce310e72d234dae550aa15c54e123f82';

    var response = await http.get(url);
    setState(() {
      isLoading = true;
    });
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      final Maps location = Maps.fromJson(jsonResponse);
      setState(() {
        locations = location.data;
        isLoading = false;
        currentPostion =
            LatLng(locations[0].station.geo[0], locations[0].station.geo[1]);
      });
    } else {
      Alert(
        context: context,
        title: "ERROR",
        desc: "Please reload this page and try again.",
        buttons: [
          DialogButton(
            child: Text(
              "OK",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            onPressed: () => Navigator.pop(context),
            width: 120,
          )
        ],
      ).show();
    }
  }

  @override
  void initState() {
    super.initState();
    _getData(searchBar);
  }

  void _setText() {
    setState(() {
      searchBar = searchBar;
    });
    _getData(searchBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[300],
        actions: <Widget>[
          IconButton(
            onPressed: () {
              setState(() {
                if (this.cusIcon.icon == Icons.search) {
                  this.cusIcon = Icon(Icons.cancel);
                  this.locationsSearchBar = TextField(
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Search City",
                    ),
                    onChanged: (value) => searchBar = value,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  );
                } else {
                  this.cusIcon = Icon(Icons.search);
                  this.locationsSearchBar = Text("Air Quality");
                  this.searchBar = '';
                }
              });
            },
            icon: cusIcon,
          ),
          // ignore: deprecated_member_use
          RaisedButton(
            onPressed: _setText,
            color: Colors.green[300],
            child: Text(
              'Search',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
        title: locationsSearchBar,
      ),
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : GoogleMap(
              mapType: MapType.normal,
              myLocationEnabled: true,
              markers: locations.length > 0
                  ? {
                      ...locations.map((e) => Marker(
                            markerId: MarkerId(e.uid.toString()),
                            icon: BitmapDescriptor.defaultMarkerWithHue(
                                BitmapDescriptor.hueCyan),
                            // icon: e.station.name,
                            position:
                                LatLng(e.station.geo[0], e.station.geo[1]),
                            infoWindow: InfoWindow(
                                title: e.station.name, snippet: e.aqi),
                          ))
                    }
                  : null,
              initialCameraPosition: CameraPosition(
                target: locations.length > 0
                    ? currentPostion
                    : LatLng(13.7563309, 100.5017651),
                zoom: 12,
              ),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
    );
  }
}
